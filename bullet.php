<?php

    class bullet{
        private $x, $y, $angle;
        private $playerIndex;
        private $liveTime = 1000;
        private $speedX = 0.9, $speedY = 0.9;
        private $color;

        public function __construct($data){
            $this->x = $data['x'];
            $this->y = $data['y'];
            $this->angle = $data['angle'];
            $this->playerIndex = $data['index'];
            $this->color = $data['color'];

            $this->speedX = $this->speedX * sin($this->angle);
            $this->speedY = $this->speedY * cos($this->angle);
        }

        public function move(){
            $this->x += $this->speedX;
            if($this->x > 800)
                $this->x = 0;
            else if($this->x < 0)
                $this->x = 800;

            $this->y += $this->speedY;
            if($this->y > 600)
                $this->y = 0;
            else if($this->y < 0)
                $this->y = 600;

            $this->liveTime--;
        }

        public function getData(){
            return array(
                'x' => $this->x,
                'y' => $this->y,
                'angle' => $this->angle,
                'index' => $this->playerIndex,
                'name' => 'bullet',
                'color' => $this->color
            );
        }

        public function getLiveTime(){
            return $this->liveTime;
        }

        public function getPosition(){
            return array('x' => $this->x, 'y' => $this->y);
        }
    }
