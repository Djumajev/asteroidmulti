<?php

    class player{
        private $x = 0, $y = 0, $angle;
        private $myIndex;
        private $speedX, $speedY;
        private $reload = false, $reloadTimer = 200;
        private $color;
        private $dead = false;
        private $sin, $cos;
        private $score = 0;

        public function __construct($myIndex, $color){
            $this->x = rand(0, 800);
            $this->y = rand(0, 600);

            $this->myIndex = $myIndex;
            $this->color = $color;
        }

        public function getPosition(){
            return array('x' => $this->x, 'y' => $this->y);
        }

        public function getIndex(){
            return $this->myIndex;
        }

        public function setPosition($coord){
            $this->x = $coord['x'];
            $this->y = $coord['y'];
        }

        public function getSpeed(){
            return array('speedX' => $this->speedX, 'speedY' => $this->speedY);
        }

        public function setSpeed($speed){
            $this->speedX = $speed['speedX'];
            $this->speedY = $speed['speedY'];
        }

        public function move(){
            $this->x += $this->speedX;
            if($this->x > 800)
                $this->x = 0;
            else if($this->x < 0)
                $this->x = 800;

            $this->y += $this->speedY;
            if($this->y > 600)
                $this->y = 0;
            else if($this->y < 0)
                $this->y = 600;

            if($this->speedY > 0)
                $this->speedY -= 0.005;
            else if($this->speedY < 0)
                $this->speedY += 0.005;

            if($this->speedX > 0)
                $this->speedX -= 0.005;
            else if($this->speedX < 0)
                $this->speedX += 0.005;

            if($this->reload && $this->reloadTimer > 0){
                $this->reloadTimer--;
            }
            else{
                $this->reload = false;
                $this->reloadTimer = 200;
            }
        }

        public function getAngle(){
            return $this->angle;
        }

        public function setAngle($angle){
            if($this->angle != $angle){
                $this->cos = cos($this->angle);
                $this->sin = sin($this->angle);
                $this->angle = $angle;
            }
        }

        public function getData(){
            return array(
                'x' => $this->x,
                'y' => $this->y,
                'angle' => $this->angle,
                'index' => $this->myIndex,
                'name' => 'player',
                'color' => $this->color,
                'dead' => $this->dead,
                'reload' => $this->reload,
                'score' => $this->score
            );
        }

        public function getReload(){
            return $this->reload;
        }

        public function shoot(){
            if(!$this->dead)
                $this->reload = true;
            return $this->getData();
        }

        public function isHit(array $bullet){
            if($bullet['index'] != $this->myIndex && !$this->dead){
                for($i = -15; $i < 15; $i++){
                    $pointA = $this->getPoint($this->x - 10, $this->y - 10);
                    $pointB = $this->getPoint($this->x + 10, $this->y - 10);
                    $pointC = $this->getPoint($this->x, $this->y + 15);

                    $a = ($pointA['x'] - $bullet['x'] + $i) * ($pointB['y'] - $pointA['y']) - ($pointB['x'] - $pointA['x']) * ($pointA['y'] - $bullet['y'] + $i);
                    $b = ($pointA['x'] - $bullet['x'] + $i) * ($pointC['y'] - $pointB['y']) - ($pointC['x'] - $pointB['x']) * ($pointB['y'] - $bullet['y'] + $i);
                    $c = ($pointA['x'] - $bullet['x'] + $i) * ($pointA['y'] - $pointC['y']) - ($pointA['x'] - $pointC['x']) * ($pointC['y'] - $bullet['y'] + $i);

                    if(($a >= 0 && $b >= 0 && $c >= 0) || ($a <= 0 && $b <= 0 && $c <= 0)){
                        $this->dead = true;
                        $this->speedX = 0;
                        $this->speddY = 0;
                        return true;
                    }
                }
            }

            return false;
        }

        private function getPoint($x, $y){
            $nx = ($this->cos * ($x - $this->x)) + ($this->sin * ($y - $this->y)) + $this->x;
            $ny = ($this->cos * ($y - $this->y)) - ($this->sin * ($x - $this->x)) + $this->y;

            return array(
                'x' => $nx,
                'y' => $ny
            );
        }

        public function respawn(){
            $this->x = rand(0, 800);
            $this->y = rand(0, 600);

            $this->dead = false;
        }

        public function plusScore(){
            $this->score++;
        }
    }
