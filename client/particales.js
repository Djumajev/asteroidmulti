class particals{
    constructor(x, y){
        this.x = x + Math.random() * (15 - 5) + 15; 
        this.y = y + Math.random() * (15 - 5) + 15;
        this.speedX = Math.random() * (1 + 1) - 1;
        this.speedY = Math.random() * (1 + 1) - 1;
        this.lifeTime = Math.random() * (1000 - 200) + 200;
    }

    move(){
        this.x += this.speedX;
        this.y += this.speedY;

        this.lifeTime--;
    }

    getPosition(){
        return {
            x: this.x,
            y: this.y
        };
    }

    getLifeTime(){
        return this.lifeTime;
    }
}
