var socket = new WebSocket("ws://localhost:8000");

//Game
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

var speedX = 0, speedY = 0;
var myX, myY, myIndex = -1, dead = false;
var shoot = false, reload = false;
var mousePosition = {x: null, y: null};
var respawn = false;

var particles = [];
var booms = [];

window.addEventListener("keydown", function (event) {
    if (event.defaultPrevented) {
      return;
    }
    switch (event.key) {
      case "ArrowDown":case "s":case "S":
            speedY = 1;
        break;
      case "ArrowUp":case "w":case "W":
            speedY = -1;
        break;
      case "ArrowLeft":case "a":case "A":
            speedX = -1;
        break;
      case "ArrowRight":case "d":case "D":
            speedX = 1;
        break;
      default:
        return;
    }
    event.preventDefault();
  }, true);

canvas.addEventListener("click",
  function(evt){
      if(!reload){
        if(!shoot){
            shoot = true;
            //TODO make BOOM
        }
      }
  }
);

canvas.addEventListener('mousemove',
  function(evt){
      mousePosition = {
        x: evt.x - canvas.offsetLeft,
        y: evt.y - canvas.offsetTop
      };
  }
);

socket.onopen = function() {
    //We connected
    console.log('Connected');
};

socket.onclose = function(event) {
    //Closed
};

socket.onmessage = function(event) {
    update(event.data);
};

socket.onerror = function(error) {
    console.log(error.message);
};      

var timerId = setInterval(function() {
    socket.send(toSend(myX, myY, shoot));
    shoot = false;
}, 0);       

function rotate(cx, cy, x, y, cos, sin) {
    let nx = (cos * (x - cx)) + (sin * (y - cy)) + cx;
    let ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
    return {
        x: nx,
        y: ny
    };
}

function update(data){
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for(let i = 0; i < particles.length - 1; i++){
        particles[i].move();
        ctx.fillStyle = "white";
        let coord = particles[i].getPosition();
        ctx.fillRect(coord.x, coord.y, 2, 2);
    }

    for(let i = 0; i < particles.length - 1; i++)
        if(particles[i].getLifeTime() <= 0)
            particles.splice(i, 1);

    data = JSON.parse(data);
    
    let scoreY = 600 - 15;
    data.forEach(element => {
        if(element.name == 'player'){
            ctx.strokeStyle = element.color;
            ctx.beginPath();

            let angle = element.angle;

            if(element.playerIndex != myIndex)
                myIndex = element.playerIndex;
            
            if(element.index == myIndex){
                myX = element.x;
                myY = element.y;
                dead = element.dead;
        
                ctx.fillStyle = "white";
                ctx.fillText('Your score: ' + element.score, 700, 15);
                ctx.fillStyle = element.color;
                ctx.fillText('Your color: ', 0, 15);
                ctx.fillRect(60, 7, 12, 12);

                if(element.dead){
                    ctx.fillStyle = "white";
                    ctx.fillText("Dead", 400, 300);
                    ctx.fillText("Click to respawn", 400, 310);
                }
            }

            ctx.fillStyle = element.color;
            ctx.fillText('Score: ' + element.score, 0, scoreY);
            scoreY -= 10;

            if(!element.dead){
                let cos = Math.cos(angle);
                let sin = Math.sin(angle);

                reload = element.reload;

                let newCoordinates = rotate(element.x, element.y, element.x - 10, element.y - 10, cos, sin);
                ctx.moveTo(newCoordinates.x, newCoordinates.y);
                newCoordinates = rotate(element.x, element.y, element.x + 10, element.y - 10, cos, sin);
                ctx.lineTo(newCoordinates.x, newCoordinates.y);
                newCoordinates = rotate(element.x, element.y, element.x, element.y + 15, cos, sin);
                ctx.lineTo(newCoordinates.x, newCoordinates.y);

                ctx.closePath();

                ctx.fillStyle = 'white';
                //TODO NICK
                ctx.fillText(element.index, element.x, element.y);

                ctx.stroke();

                let i = booms.indexOf(element.index);
                if(i >= 0)    
                    booms.splice(i, 1);
            }
            else {
                if(booms.indexOf(element.index) < 0){
                    console.log('Boom!');
                    for(let i = 0; i < 40; i++)
                        particles.push(new particals(element.x, element.y));

                    booms.push(element.index);
                }
            }
        }
        else if(element.name == 'bullet'){
            ctx.fillStyle = element.color;
            ctx.fillRect(element.x,element.y,4,4);
        }
    });

}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function toSend(x, y, shoot){
    let angle = Math.atan2(
        mousePosition.x - x,
        mousePosition.y - y
    );
    if(angle < 0){
        angle += (2 * Math.PI);
    }

    let output;
    if(!dead){
        output ={
            'speedX': speedX,
            'speedY': speedY,
            'angle': angle,
            'shoot': shoot,
            'respawn': false
        };

        speedX = 0;
        speedY = 0;

        return JSON.stringify(output);
    }

    speedX = 0;
    speedY = 0;

    output ={
        'speedX': speedX,
        'speedY': speedY,
        'angle': false,
        'shoot': false,
        'respawn': shoot
    };

    return JSON.stringify(output);
}
