<?php
    include_once 'player.php';
    include_once 'bullet.php';

    class mainGame{
        private $players = array(),
            $bullets = array();
        private $maxSpeed = 1;
        private $colors = array(
            'yellow',
            'blue'
        );

        public function __construct(){
            echo 'new room created'.PHP_EOL;
        }

        public function getWorld($data, $index){
            $output = array();
            $data = json_decode($data, true);

            if(count($this->players) == 0)
                return $output;

            $this->players[$index]->setAngle(round($data['angle'], 2));
            $speedArr = $this->players[$index]->getSpeed();

            if($data['shoot']){
                if(!$this->players[$index]->getReload()){
                    $this->bullets[] = new bullet($this->players[$index]->shoot());
                }
            }

            if($data['respawn']){
                $this->players[$index]->respawn();
            }
            
            //if($this->checkSpeed($speedArr['speedX'], $data['speedX']))
                if($data['speedX'] > 0)    
                    $speedArr['speedX'] = $this->maxSpeed;
                else if($data['speedX'] < 0)
                    $speedArr['speedX'] = -$this->maxSpeed;

            //if($this->checkSpeed($speedArr['speedY'], $data['speedY']))
                if($data['speedY'] > 0)    
                    $speedArr['speedY'] = $this->maxSpeed;
                else if($data['speedY'] < 0)
                    $speedArr['speedY'] = -$this->maxSpeed;

            $this->players[$index]->setSpeed($speedArr);

            foreach($this->players as $player){
                $output[] = array_merge($player->getData(), array('playerIndex' => $index));
            }

            foreach($this->bullets as $bullet){
                $output[] = $bullet->getData();
            }

            return json_encode($output);
        }

        public function newPlayer(){
            $this->players[] = new player(count($this->players), $this->colors[count($this->players)]);
            return end($this->players)->getIndex();
        }

        public function deletePlayer($index){
            unset($this->players[$index]);
        }

        public function tick(){
            if(count($this->players) == 0)
                return;

            foreach($this->players as $player){
                $player->move();
            }

            foreach($this->bullets as $bullet){
                $bullet->move();
                if($bullet->getLiveTime() <= 0){
                    unset($this->bullets[array_search($bullet, $this->bullets)]);
                }
            }

            //TODO OPTIMAZATION
            foreach($this->players as $player){
                foreach($this->bullets as $bullet){
                    $bulletArr = $bullet->getData();
                    $playerArr = $player->getData();

                    if(sqrt(($bulletArr['x'] - $playerArr['x'])*($bulletArr['x'] - $playerArr['x']) + ($bulletArr['y'] - $playerArr['y'])*($bulletArr['y'] - $playerArr['y']) < 100))
                        if($player->isHit($bulletArr)){
                            $index = $bulletArr['index'];

                            unset($this->bullets[array_search($bullet, $this->bullets)]);
                            $this->players[$index]->plusScore();
                        }
                }
            }
        }

        /*private function checkSpeed($fisrt, $second){
            if(abs($fisrt + $second) <= $this->maxSpeed)
                return true;
            return false;
        }*/

    }
